/*-------------------------------------------------------------------------
Significant portions of this project are based on the Ogre Tutorials
- https://ogrecave.github.io/ogre/api/1.10/tutorials.html
Copyright (c) 2000-2013 Torus Knot Software Ltd

Manual generation of meshes from here:
- http://wiki.ogre3d.org/Generating+A+Mesh

*/

#include <exception>
#include <iostream>

#include "Game.h"
#include "SoundDevice.h"
#include "SoundBuffer.h"
#include "SoundSource.h"


Game::Game() : ApplicationContext("OgreTutorialApp")
{
    dynamicsWorld = NULL;
    wDown = false;
    aDown = false;
    sDown = false;
    dDown = false;
    mDown = false; // mission breif key
    
    ogreDebugDraw = true;
    bulletDebugDraw = true;
}


Game::~Game()
{
    //cleanup in the reverse order of creation/initialization

    ///-----cleanup_start----
    //remove the rigidbodies from the dynamics world and delete them

    for (int i = dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
    {
        btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[i];
        btRigidBody* body = btRigidBody::upcast(obj);

        if (body && body->getMotionState())
        {
            delete body->getMotionState();
        }

        dynamicsWorld->removeCollisionObject(obj);
        delete obj;
    }

    //delete collision shapes
    for (int j = 0; j < collisionShapes.size(); j++)
    {
        btCollisionShape* shape = collisionShapes[j];
        collisionShapes[j] = 0;
        delete shape;
    }

    //delete dynamics world
    delete dynamicsWorld;

    //delete solver
    delete solver;

    //delete broadphase
    delete overlappingPairCache;

    //delete dispatcher
    delete dispatcher;

    delete collisionConfiguration;

    //next line is optional: it will be cleared by the destructor when the array goes out of scope
    collisionShapes.clear();
}


void Game::setup()
{
    // do not forget to call the base first
    ApplicationContext::setup();

    addInputListener(this);

    // get a pointer to the already created root
    Root* root = getRoot();
    scnMgr = root->createSceneManager();

    // register our scene with the RTSS
    RTShader::ShaderGenerator* shadergen = RTShader::ShaderGenerator::getSingletonPtr();
    shadergen->addSceneManager(scnMgr);

    bulletInit();

    //Debug drawing
    dbgdraw = new BtOgre::DebugDrawer(scnMgr->getRootSceneNode(), dynamicsWorld);
    dynamicsWorld->setDebugDrawer(dbgdraw);
    dbgdraw->setDebugMode(bulletDebugDraw);

    setupCamera();

    setupPlayer();

    setupFloor();

    setupLights();

   // setupBoxMesh();
    //buildings
    setupBuildMesh();
    setupBuildMesh1();
  
    setupBoulderMesh();

    setupMapMesh();
}


void Game::setupPlayer()
{
    SceneNode* sceneRoot = scnMgr->getRootSceneNode();
    float mass = 1.0f;

    // Axis
    Vector3 axis(1.0, 80.0, 0.0);
    axis.normalise();

    //angle
    Radian rads(Degree(90.0));

    player = new Player();
    player->createMesh(scnMgr);
    player->attachToNode(sceneRoot);

    player->setRotation(axis, rads);
    
 
    player->setPosition(20.0f, 100.0f, 20.0f);

    /*thisSceneNode->_updateBounds();
    const AxisAlignedBox& b = thisSceneNode->_getWorldAABB(); // box->getWorldBoundingBox();
    thisSceneNode->showBoundingBox(ogreDebugDraw);*/

    player->createRigidBody(mass);
    player->addToCollisionShapes(collisionShapes);
    player->addToDynamicsWorld(dynamicsWorld);
}

void Game::setupCamera()
{
    // Create Camera
    Camera* cam = scnMgr->createCamera("myCam");

    //Setup Camera
    cam->setNearClipDistance(5);

    // Position Camera - to do this it must be attached to a scene graph and added
    // to the scene.
    SceneNode* camNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    camNode->setPosition(0, 300, 800);
    camNode->lookAt(Vector3(0, 0, 0), Node::TransformSpace::TS_WORLD);
    camNode->attachObject(cam);

    // Setup viewport for the camera.
    Viewport* vp = getRenderWindow()->addViewport(cam);
    vp->setBackgroundColour(ColourValue(0, 0, 0));

    // link the camera and view port.
    cam->setAspectRatio(Real(vp->getActualWidth()) / Real(vp->getActualHeight()));
  
   
}

void Game::bulletInit()
{
    ///collision configuration contains default setup for memory, collision setup. Advanced users can create their own configuration.
    collisionConfiguration = new btDefaultCollisionConfiguration();

    ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
    dispatcher = new btCollisionDispatcher(collisionConfiguration);

    ///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
    overlappingPairCache = new btDbvtBroadphase();

    ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
    solver = new btSequentialImpulseConstraintSolver;

    dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);

    dynamicsWorld->setGravity(btVector3(0, -10, 0));
}

void Game::setupBoxMesh()
{
    Entity* box = scnMgr->createEntity("cube.mesh");
    box->setCastShadows(true);

    SceneNode* thisSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    thisSceneNode->attachObject(box);
    box->setMaterialName("Examples/Level");
    // Axis
    Vector3 axis(1.0, 1.0, 0.0);
    axis.normalise();

    //angle
    Radian rads(Degree(60.0));

    //quat from axis angle
    Quaternion quat(rads, axis);

    // thisSceneNode->setOrientation(quat);
    thisSceneNode->setScale(1.0, 1.0, 1.0);

    //get bounding box here.
    thisSceneNode->_updateBounds();
    const AxisAlignedBox& b = thisSceneNode->_getWorldAABB(); // box->getWorldBoundingBox();
    thisSceneNode->showBoundingBox(ogreDebugDraw);
    // std::cout << b << std::endl;
    // std::cout << "AAB [" << (float)b.x << " " << b.y << " " << b.z << "]" << std::endl;

    // Now I have a bounding box I can use it to make the collision shape.
    // I'll rotate the scene node and later the collision shape.
    thisSceneNode->setOrientation(quat);

    thisSceneNode->setPosition(0, 200, 0);

    Vector3 meshBoundingBox(b.getSize());

    if (meshBoundingBox == Vector3::ZERO)
    {
        std::cout << "bounding voluem size is zero." << std::endl;
    }

    //create a dynamic rigidbody

    btCollisionShape* colShape = new btBoxShape(btVector3(meshBoundingBox.x / 1.0f, meshBoundingBox.y / 1.0f, meshBoundingBox.z / 1.0f));
    std::cout << "Mesh box col shape [" << (float)meshBoundingBox.x << " " << meshBoundingBox.y << " " << meshBoundingBox.z << "]" << std::endl;
    // btCollisionShape* colShape = new btBoxShape(btVector3(10.0,10.0,10.0));
    //btCollisionShape* colShape = new btSphereShape(btScalar(1.));
    collisionShapes.push_back(colShape);

    /// Create Dynamic Objects
    btTransform startTransform;
    startTransform.setIdentity();

    //startTransform.setOrigin(btVector3(0, 200, 0));

    Vector3 pos = thisSceneNode->_getDerivedPosition();
    startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

    Quaternion quat2 = thisSceneNode->_getDerivedOrientation();
    startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

    btScalar mass(1.f);

    //rigidbody is dynamic if and only if mass is non zero, otherwise static
    bool isDynamic = (mass != 0.f);

    btVector3 localInertia(0, 0, 0);
    if (isDynamic)
    {
        // Debugging
        //std::cout << "I see the cube is dynamic" << std::endl;
        colShape->calculateLocalInertia(mass, localInertia);
    }

    std::cout << "Local inertia [" << (float)localInertia.x() << " " << localInertia.y() << " " << localInertia.z() << "]" << std::endl;

    //using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
    btRigidBody* body = new btRigidBody(rbInfo);

    //Link to ogre
    body->setUserPointer((void*)thisSceneNode);

    //  body->setRestitution(0.5);

    dynamicsWorld->addRigidBody(body);
}

void Game::setupBuildMesh()
{
    Entity* build = scnMgr->createEntity("house.mesh");
    build->setCastShadows(true);

    SceneNode* thisSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    thisSceneNode->attachObject(build);
    build->setMaterialName("Examples/House");
    // Axis
    Vector3 axis(1.0, 1.0, 0.0);
    axis.normalise();

    //angle
    Radian rads(Degree(.0));

    //quat from axis angle
    Quaternion quat(rads, axis);

    // thisSceneNode->setOrientation(quat);
    thisSceneNode->setScale(8.0, 8.0, 8.0);


    thisSceneNode->setPosition(225, 100, 450); //L&R, Up&Down, F&B

    //get bounding box here.
    thisSceneNode->_updateBounds();
    const AxisAlignedBox& b = thisSceneNode->_getWorldAABB(); // box->getWorldBoundingBox();
    // debug draw is commented out to not show collisions with static objects ////////////////////////////////////////////////////////////////////////////
    thisSceneNode->showBoundingBox(ogreDebugDraw);
 
    // std::cout << b << std::endl;
    // std::cout << "AAB [" << (float)b.x << " " << b.y << " " << b.z << "]" << std::endl;
   Vector3 meshBoundingBox(b.getSize());

    if (meshBoundingBox == Vector3::ZERO)
    {
        std::cout << "bounding voluem size is zero." << std::endl;
    }

    //create a dynamic rigidbody
    // Mesh bounding size needed to be increased to mak ethe collisions suit wioth teh object box
    btCollisionShape* colShape = new btBoxShape(btVector3(meshBoundingBox.x / 7.5f, meshBoundingBox.y / 7.5f, meshBoundingBox.z / 7.5f));
    std::cout << "Mesh box col shape [" << (float)meshBoundingBox.x << " " << meshBoundingBox.y << " " << meshBoundingBox.z << "]" << std::endl;
    // btCollisionShape* colShape = new btBoxShape(btVector3(10.0,10.0,10.0));
    //btCollisionShape* colShape = new btSphereShape(btScalar(1.));
    collisionShapes.push_back(colShape);

    /// Create Dynamic Objects
    btTransform startTransform;
    startTransform.setIdentity();

    //startTransform.setOrigin(btVector3(0, 200, 0));

    Vector3 pos = thisSceneNode->_getDerivedPosition();
    startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

    Quaternion quat2 = thisSceneNode->_getDerivedOrientation();
    startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

    btScalar mass(0.f);

    //rigidbody is dynamic if and only if mass is non zero, otherwise static
    bool isDynamic = (mass != 0.f);

    btVector3 localInertia(0, 0, 0);
    if (isDynamic)
    {
        // Debugging
        //std::cout << "I see the cube is dynamic" << std::endl;
        colShape->calculateLocalInertia(mass, localInertia);
    }

    std::cout << "Local inertia [" << (float)localInertia.x() << " " << localInertia.y() << " " << localInertia.z() << "]" << std::endl;

    //using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
    btRigidBody* body = new btRigidBody(rbInfo);

    //Link to ogre
    body->setUserPointer((void*)thisSceneNode);

    //  body->setRestitution(0.5);

    dynamicsWorld->addRigidBody(body);
    // Now I have a bounding box I can use it to make the collision shape.
    // I'll rotate the scene node and later the collision shape.
    thisSceneNode->setOrientation(quat);
 
}

void Game::setupBuildMesh1()
{
    Entity* build = scnMgr->createEntity("house.mesh");
    build->setCastShadows(true);

    SceneNode* thisSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    thisSceneNode->attachObject(build);
    build->setMaterialName("Examples/House");
    // Axis
    Vector3 axis(1.0, 1.0, 0.0);
    axis.normalise();

    //angle
    Radian rads(Degree(.0));

    //quat from axis angle
    Quaternion quat(rads, axis);

    // thisSceneNode->setOrientation(quat);
    thisSceneNode->setScale(15.0, 15.0, 15.0);


    thisSceneNode->setPosition(-550, 100, -400);
    //get bounding box here.
    thisSceneNode->_updateBounds();
    const AxisAlignedBox& b = thisSceneNode->_getWorldAABB(); // box->getWorldBoundingBox();
    // debug draw is commented out to not show collisions with static objects ////////////////////////////////////////////////////////////////////////////
    //thisSceneNode->showBoundingBox(ogreDebugDraw);
    thisSceneNode->showBoundingBox(ogreDebugDraw);
    // std::cout << b << std::endl;
    // std::cout << "AAB [" << (float)b.x << " " << b.y << " " << b.z << "]" << std::endl;
 Vector3 meshBoundingBox(b.getSize());

    if (meshBoundingBox == Vector3::ZERO)
    {
        std::cout << "bounding voluem size is zero." << std::endl;
    }

    //create a dynamic rigidbody

    btCollisionShape* colShape = new btBoxShape(btVector3(meshBoundingBox.x / 3.0f, meshBoundingBox.y / 3.0f, meshBoundingBox.z / 3.0f));
    std::cout << "Mesh box col shape [" << (float)meshBoundingBox.x << " " << meshBoundingBox.y << " " << meshBoundingBox.z << "]" << std::endl;
    // btCollisionShape* colShape = new btBoxShape(btVector3(10.0,10.0,10.0));
    //btCollisionShape* colShape = new btSphereShape(btScalar(1.));
    collisionShapes.push_back(colShape);

    /// Create Dynamic Objects
    btTransform startTransform;
    startTransform.setIdentity();

    //startTransform.setOrigin(btVector3(0, 200, 0));

    Vector3 pos = thisSceneNode->_getDerivedPosition();
    startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

    Quaternion quat2 = thisSceneNode->_getDerivedOrientation();
    startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

    btScalar mass(1.f);

    //rigidbody is dynamic if and only if mass is non zero, otherwise static
    bool isDynamic = (mass != 0.f);

    btVector3 localInertia(0, 0, 0);
    if (isDynamic)
    {
        // Debugging
        //std::cout << "I see the cube is dynamic" << std::endl;
        colShape->calculateLocalInertia(mass, localInertia);
    }

    std::cout << "Local inertia [" << (float)localInertia.x() << " " << localInertia.y() << " " << localInertia.z() << "]" << std::endl;

    //using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
    btRigidBody* body = new btRigidBody(rbInfo);

    //Link to ogre
    body->setUserPointer((void*)thisSceneNode);

    //  body->setRestitution(0.5);

    dynamicsWorld->addRigidBody(body);
    // Now I have a bounding box I can use it to make the collision shape.
    // I'll rotate the scene node and later the collision shape.
    thisSceneNode->setOrientation(quat);
   

}


void Game::setupBoulderMesh()
{
    Entity* boulder = scnMgr->createEntity("boulder.mesh");
    boulder->setCastShadows(true);

    SceneNode* thisSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    thisSceneNode->attachObject(boulder);
    boulder->setMaterialName("Examples/Boulder");
    // Axis
    Vector3 axis(1.0, 1.0, 0.0);
    axis.normalise();

    //angle
    Radian rads(Degree(.0));

    //quat from axis angle
    Quaternion quat(rads, axis);

    // thisSceneNode->setOrientation(quat);
    thisSceneNode->setScale(2.0, 2.0, 2.0);


    thisSceneNode->setPosition(-460, 400, -400);
    //get bounding box here.
    thisSceneNode->_updateBounds();
    const AxisAlignedBox& b = thisSceneNode->_getWorldAABB(); // box->getWorldBoundingBox();
    // debug draw is commented out to not show collisions with static objects ////////////////////////////////////////////////////////////////////////////
    //thisSceneNode->showBoundingBox(ogreDebugDraw);
    thisSceneNode->showBoundingBox(ogreDebugDraw);
    // std::cout << b << std::endl;
    // std::cout << "AAB [" << (float)b.x << " " << b.y << " " << b.z << "]" << std::endl;
    Vector3 meshBoundingBox(b.getSize());

    if (meshBoundingBox == Vector3::ZERO)
    {
        std::cout << "bounding voluem size is zero." << std::endl;
    }

    //create a dynamic rigidbody

    btCollisionShape* colShape = new btBoxShape(btVector3(meshBoundingBox.x / 4.5f, meshBoundingBox.y / 4.5f, meshBoundingBox.z / 4.5f));
    std::cout << "Mesh box col shape [" << (float)meshBoundingBox.x << " " << meshBoundingBox.y << " " << meshBoundingBox.z << "]" << std::endl;
    // btCollisionShape* colShape = new btBoxShape(btVector3(10.0,10.0,10.0));
    //btCollisionShape* colShape = new btSphereShape(btScalar(1.));
    collisionShapes.push_back(colShape);

    /// Create Dynamic Objects
    btTransform startTransform;
    startTransform.setIdentity();

    //startTransform.setOrigin(btVector3(0, 200, 0));

    Vector3 pos = thisSceneNode->_getDerivedPosition();
    startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

    Quaternion quat2 = thisSceneNode->_getDerivedOrientation();
    startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));

    btScalar mass(100.f);

    //rigidbody is dynamic if and only if mass is non zero, otherwise static
    bool isDynamic = (mass != 0.f);

    btVector3 localInertia(10, 0, 0);
    if (isDynamic)
    {
        // Debugging
        //std::cout << "I see the cube is dynamic" << std::endl;
        colShape->calculateLocalInertia(mass, localInertia);
    }

    std::cout << "Local inertia [" << (float)localInertia.x() << " " << localInertia.y() << " " << localInertia.z() << "]" << std::endl;

    //using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
    btRigidBody* body = new btRigidBody(rbInfo);

    //Link to ogre
    body->setUserPointer((void*)thisSceneNode);

    //  body->setRestitution(0.5);

    dynamicsWorld->addRigidBody(body);
    // Now I have a bounding box I can use it to make the collision shape.
    // I'll rotate the scene node and later the collision shape.
    thisSceneNode->setOrientation(quat);


}
void Game::setupMapMesh()
{
    Entity* map = scnMgr->createEntity("Level.mesh");
    map->setCastShadows(true);

    SceneNode* thisSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    thisSceneNode->attachObject(map);
    /////////////******************************************************************************************
    // For each material I changed the example config files they have the location of which my texture image was located 
    // this allowed me to then attach this material to my entity. i made sure to do this in the main src folder and noty the build to make sure it was not overwrriten when the install is debuged
    /////////////*****************************************************************************************
    map->setMaterialName("Examples/Level");
    // Axis
    Vector3 axis(10.0, 0.0, 0.0);
    axis.normalise();

    //angle
    Radian rads(Degree(-90.0));

    //quat from axis angle
    Quaternion quat(rads, axis);

    // thisSceneNode->setOrientation(quat);
    thisSceneNode->setScale(25.0, 35.0, 25.0);

    //get bounding box here.
    thisSceneNode->_updateBounds();
    const AxisAlignedBox& b = thisSceneNode->_getWorldAABB(); // box->getWorldBoundingBox();
  
    // std::cout << b << std::endl;
    // std::cout << "AAB [" << (float)b.x << " " << b.y << " " << b.z << "]" << std::endl;

    // Now I have a bounding box I can use it to make the collision shape.
    // I'll rotate the scene node and later the collision shape.
    thisSceneNode->setOrientation(quat);

    thisSceneNode->setPosition(0, 1, 20);


    /// Create Dynamic Objects
    btTransform startTransform;
    startTransform.setIdentity();

    //startTransform.setOrigin(btVector3(0, 200, 0));

    Vector3 pos = thisSceneNode->_getDerivedPosition();
    startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

    Quaternion quat2 = thisSceneNode->_getDerivedOrientation();
    startTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));



    // keep the mass at static to make sure it stay still but can still be collieded with
    // the floor is kept as static as we downt wnat it to be subject to gravity
    btScalar mass(0.f);

    //rigidbody is dynamic if and only if mass is non zero, otherwise static
    bool isDynamic = (mass != 0.f);

    btVector3 localInertia(0, 0, 0);
   
    std::cout << "Local inertia [" << (float)localInertia.x() << " " << localInertia.y() << " " << localInertia.z() << "]" << std::endl;
}

void Game::setupFloor()
{

    // Create a plane
    Plane plane(Vector3::UNIT_Y, 0);

    // Define the plane mesh
    MeshManager::getSingleton().createPlane(
        "ground", RGN_DEFAULT,
        plane,
        1500, 1500, 20, 20,
        true,
        1, 5, 5,
        Vector3::UNIT_Z);

    // Create an entity for the ground
    Entity* groundEntity = scnMgr->createEntity("ground");

    //Setup ground entity
    // Shadows off
    groundEntity->setCastShadows(false);

    // Material - Examples is the rsources file,
    // Rockwall (texture/properties) is defined inside it.
    groundEntity->setMaterialName("Examples/Floor");

    // Create a scene node to add the mesh too.
    SceneNode* thisSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    thisSceneNode->attachObject(groundEntity);

    //the ground is a cube of side 100 at position y = 0.
       //the sphere will hit it at y = -6, with center at -5
    btCollisionShape* groundShape = new btBoxShape(btVector3(btScalar(750.), btScalar(50.), btScalar(750.)));

    collisionShapes.push_back(groundShape);

    btTransform groundTransform;
    groundTransform.setIdentity();
    //  groundTransform.setOrigin(btVector3(0, -100, 0));

    Vector3 pos = thisSceneNode->_getDerivedPosition();

    //Box is 100 deep (dimensions are 1/2 heights)
    //but the plane position is flat.
    groundTransform.setOrigin(btVector3(pos.x, pos.y -50.0, pos.z));

    Quaternion quat2 = thisSceneNode->_getDerivedOrientation();
    groundTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));


    btScalar mass(0.);

    //rigidbody is dynamic if and only if mass is non zero, otherwise static
    bool isDynamic = (mass != 0.f);

    btVector3 localInertia(0, 0, 0);
    if (isDynamic)
        groundShape->calculateLocalInertia(mass, localInertia);

    //using motionstate is optional, it provides interpolation capabilities, and only synchronizes 'active' objects
    btDefaultMotionState* myMotionState = new btDefaultMotionState(groundTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, groundShape, localInertia);
    btRigidBody* body = new btRigidBody(rbInfo);

    //   body->setRestitution(0.0);

    //add the body to the dynamics world
    dynamicsWorld->addRigidBody(body);
}

bool Game::frameStarted(const Ogre::FrameEvent& evt)
{
    //Be sure to call base class - otherwise events are not polled.
    

    if (this->dynamicsWorld != NULL)
    {
        // Apply new forces
        if (wDown)
        {
            player->Forward();
           
        }
        if (aDown)
        {
            player->Right();
            //player->spinRight(); // -- Doesnt' work
        }
        if (sDown)
        {
            player->Backward();

        }
        if (dDown)
        {
            player->Left();

        }
        if (mDown)
        {
            player->soundPlay();
        }
        // Bullet can work with a fixed timestep
        //dynamicsWorld->stepSimulation(1.f / 60.f, 10);

        // Or a variable one, however, under the hood it uses a fixed timestep
        // then interpolates between them.

        dynamicsWorld->stepSimulation((float)evt.timeSinceLastFrame, 10);

        // update positions of all objects
        for (int j = dynamicsWorld->getNumCollisionObjects() - 1; j >= 0; j--)
        {
            btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[j];
            btRigidBody* body = btRigidBody::upcast(obj);
            btTransform trans;

            if (body && body->getMotionState())
            {
                body->getMotionState()->getWorldTransform(trans);

                /* https://oramind.com/ogre-bullet-a-beginners-basic-guide/ */
                void* userPointer = body->getUserPointer();
                // Player should know enough to update itself.
                if (userPointer == player)
                {
                    // Ignore player, he's always updated!
                }
                else //This is just to keep the other objects working.
                {
                    if (userPointer)
                    {
                        btQuaternion orientation = trans.getRotation();
                        Ogre::SceneNode* sceneNode = static_cast<Ogre::SceneNode*>(userPointer);
                        sceneNode->setPosition(Ogre::Vector3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
                        sceneNode->setOrientation(Ogre::Quaternion(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));
                    }

                }

            }
            else
            {
                trans = obj->getWorldTransform();
            }

        }

        this->dynamicsWorld->debugDrawWorld();

        this->dbgdraw->step();

        player->update();
      

    }
    return true;
};



bool Game::frameEnded(const Ogre::FrameEvent& evt)
{
    if (this->dynamicsWorld != NULL)
    {
        // Bullet can work with a fixed timestep
        //dynamicsWorld->stepSimulation(1.f / 60.f, 10);

        // Or a variable one, however, under the hood it uses a fixed timestep
        // then interpolates between them.

        dynamicsWorld->stepSimulation((float)evt.timeSinceLastFrame, 10);
        ApplicationContext::frameStarted(evt);
        //SoundDevice* mysounddevice = SoundDevice::get();

        //uint32_t /*ALuint*/ sound1 = SoundBuffer::get()->addSoundEffect("E:/Orge/tankgame-calumdavid/build/res/sounds/Tank1.ogg");
        //uint32_t /*ALuint*/ sound2 = SoundBuffer::get()->addSoundEffect("E:/Orge/tankgame-calumdavid/build/res/sounds/spell.ogg");

        //SoundSource mySpeaker;

        //mySpeaker.Play(sound1);
        //mySpeaker.Play(sound2);
        //return 1;
    }
    return true;
}


void Game::setupLights()
{
    // Setup Abient light
    scnMgr->setAmbientLight(ColourValue(0, 0, 0));
    scnMgr->setShadowTechnique(ShadowTechnique::SHADOWTYPE_STENCIL_MODULATIVE);

    // Add a spotlight
    Light* spotLight = scnMgr->createLight("SpotLight");

    // Configure
    spotLight->setDiffuseColour(0, 0, 0.0);
    spotLight->setSpecularColour(0, 0, 0.0);
    spotLight->setType(Light::LT_SPOTLIGHT);
    spotLight->setSpotlightRange(Degree(35), Degree(50));


    // Create a schene node for the spotlight
    SceneNode* spotLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    spotLightNode->setDirection(-1, -1, 0);
    spotLightNode->setPosition(Vector3(200, 200, 0));

    // Add spotlight to the scene node.
    spotLightNode->attachObject(spotLight);

    // Create directional light
    Light* directionalLight = scnMgr->createLight("DirectionalLight");

    // Configure the light
    directionalLight->setType(Light::LT_DIRECTIONAL);
    directionalLight->setDiffuseColour(ColourValue(0.66, 0.44, 0.14));
    directionalLight->setSpecularColour(ColourValue(0.4, 0, 0));

    // Setup a scene node for the directional lightnode.
    SceneNode* directionalLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    directionalLightNode->attachObject(directionalLight);
    directionalLightNode->setDirection(Vector3(0, -1, 1));

    // Create a point light
    Light* pointLight = scnMgr->createLight("PointLight");

    // Configure the light
    pointLight->setType(Light::LT_POINT);
    pointLight->setDiffuseColour(0.3, 0.3, 0.3);
    pointLight->setSpecularColour(0.3, 0.3, 0.3);

    // setup the scene node for the point light
    SceneNode* pointLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();

    // Configure the light
    pointLightNode->setPosition(Vector3(0, 150, 250));

    // Add the light to the scene.
    pointLightNode->attachObject(pointLight);

}

bool Game::keyPressed(const KeyboardEvent& evt)
{
    std::cout << "Got key event" << std::endl;
    if (evt.keysym.sym == SDLK_ESCAPE)
    {
        getRoot()->queueEndRendering();
    }
    if (evt.keysym.sym == 'w')
    {
        wDown = true;
        
        
    }

    if (evt.keysym.sym == 'a')
    {
        aDown = true;
    }

    if (evt.keysym.sym == 's')
    {
        sDown = true;

    }

    if (evt.keysym.sym == 'd')
    {
        dDown = true;
    }
    if (evt.keysym.sym == 'm')
    {
        mDown = true;
    }
    return true;
    // this key is wat starts the movemnt system and debug draw. //////////////////////////////////////////////////////////
    if (evt.keysym.sym == 'w')
    {
        bulletDebugDraw = !bulletDebugDraw;
        this->dbgdraw->setDebugMode(bulletDebugDraw);
    }

    return true;
}// Keyboard events
bool Game::keyReleased(const KeyboardEvent& evt)// keyboard events
{
    std::cout << "Got key up event" << std::endl;

    if (evt.keysym.sym == 'w')
    {
        wDown = false;
    }

    if (evt.keysym.sym == 'a')
    {
        aDown = false;
    }
    if (evt.keysym.sym == 's')
    {
        sDown = false;

    }

    if (evt.keysym.sym == 'd')
    {
        dDown = false;
    }

    if (evt.keysym.sym == 'm')
    {
        mDown = false;
    }
    return true;
}


bool Game::mouseMoved(const MouseMotionEvent& evt)
{
    std::cout << "Got Mouse" << std::endl;
    return true;
}
