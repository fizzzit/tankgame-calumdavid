#pragma once

#include "Ogre.h"
#include "OgreApplicationContext.h"
#include "OgreInput.h"
#include "OgreRTShaderSystem.h"
#include "OgreApplicationContext.h"
#include "OgreCameraMan.h"

/* Bullet3 Physics */
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"

/* BtOgre's debug draw */
#include "BtOgrePG.h"
#include "BtOgreGP.h"
#include "BtOgreExtras.h"

using namespace Ogre;
using namespace OgreBites;

#include "Player.h"

class Game : public ApplicationContext, public InputListener
{
private:
    SceneManager* scnMgr;

    //// collision configuration.
    btDefaultCollisionConfiguration* collisionConfiguration = new btDefaultCollisionConfiguration();

    ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
    btCollisionDispatcher* dispatcher = new btCollisionDispatcher(collisionConfiguration);

    ///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
    btBroadphaseInterface* overlappingPairCache = new btDbvtBroadphase();

    ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
    btSequentialImpulseConstraintSolver* solver = new btSequentialImpulseConstraintSolver;
    btDiscreteDynamicsWorld* dynamicsWorld;

    ///keep track of the shapes, we release memory at exit.
    //make sure to re-use collision shapes among rigid bodies whenever possible!
    btAlignedObjectArray<btCollisionShape*> collisionShapes;

    //Debugging.
    BtOgre::DebugDrawer* dbgdraw;

    //Debugging flags.
    bool ogreDebugDraw;
    bool bulletDebugDraw;

    //derclare player 
    Player* player;
    // playuer keboard events declared
    bool wDown;

    bool aDown;

    bool sDown;

    bool dDown;

    bool mDown;
public:
    Game();
    virtual ~Game();

    void setup();

    void setupCamera();
    // Here i have declared the class i want to use, the builds and level are declared here.
    void setupBoxMesh();
    void setupBuildMesh();
    void setupBuildMesh1();
    void setupBoulderMesh();
    void setupSound();
    void setupPlayer();
    void setupMapMesh();
    void setupFloor();

    void setupLights();
    //keyboard events declared
    bool keyPressed(const KeyboardEvent& evt);

    bool keyReleased(const KeyboardEvent& evt);

    bool mouseMoved(const MouseMotionEvent& evt);

    bool frameStarted(const FrameEvent& evt);

    bool frameEnded(const FrameEvent& evt);

    void bulletInit();
};
